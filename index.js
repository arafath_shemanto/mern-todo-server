const express = require("express");
const app = express();
const cors = require("cors");
const port = process.env.PORT || 5000;
const { MongoClient, ServerApiVersion, ObjectId } = require("mongodb");

// middle ware
app.use(cors());
app.use(express.json());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
// request here
// todo
// NJoN6pEfdstjzPdy
const uri =
  "mongodb+srv://todo:NJoN6pEfdstjzPdy@cluster0.xuzwk.mongodb.net/?retryWrites=true&w=majority";
const client = new MongoClient(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  serverApi: ServerApiVersion.v1,
});
async function run() {
  try {
    await client.connect();
    const todoCollection = client.db("mern-todo").collection("todos");
    // get todo
    app.get("/todos", async (req, res) => {
      const query = {};
      const cursor = todoCollection.find(query);
      const result = await cursor.toArray();
      res.send(result);
    });

    // insert one
    app.post("/todos", async (req, res) => {
      const data = req.body;
      console.log(data);
      const result = await todoCollection.insertOne(data);
      res.send(result);
    });

    // update  one
    app.put("/todos/:id", async (req, res) => {
      const id = req.params.id;
      const data = req.body;
      console.log(data);
      const options = { upsert: true };
      const filter = { _id: ObjectId(id) };
      const updateDoc = {
        $set: {
          ...data,
        },
      };
      const result = await todoCollection.updateOne(filter, updateDoc, options);
      res.send(result);
    });

    // update  one
    app.put("/todos/complete_task/:id", async (req, res) => {
      const id = req.params.id;
      const filter = { _id: ObjectId(id) };
      const updateDoc = {
        $set: { role: "complete" },
      };
      const result = await todoCollection.updateOne(filter, updateDoc);
      res.send(result);
    });
  } finally {
  }
}
run().catch(console.dir());
// end every thing
app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.listen(port, () => {
  console.log(`todo listening on port ${port}`);
});
